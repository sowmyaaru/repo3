import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

//https://www.seleniumeasy.com/test/basic-first-form-demo.html
public class BasicFirstformdemo {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
System.setProperty("webdriver.gecko.driver","C:\\selenium\\geckodriver.exe");
WebDriver driver=new FirefoxDriver();
String url="https://www.seleniumeasy.com/test/basic-first-form-demo.html\r\n" ;
driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
driver.get(url);
driver.findElement(By.id("at-cv-lightbox-close")).click();
driver.findElement(By.id("user-message")).sendKeys("hai selenium");
//Thread.sleep(30000);
driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[2]/form/button")).click();
System.out.println("message displayed");
	
//driver.findElement(By.cssSelector("button.btn btn-default")).click();
driver.findElement(By.id("sum1")).sendKeys("90");
driver.findElement(By.id("sum2")).sendKeys("90");
driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div[2]/form/button")).click();

	//driver.close();
	
	}
}
